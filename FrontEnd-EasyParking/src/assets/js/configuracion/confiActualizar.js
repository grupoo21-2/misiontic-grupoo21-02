function actualizarConfi(){
    const codigo2 = document.getElementById("codi").value;
    const nombreparc = document.getElementById("nomparck").value;
    const plazcarr= document.getElementById("plazca").value;
    const plazmot= document.getElementById("plazmo").value;
    const prefactu= document.getElementById("prefac").value;
    const numerini= document.getElementById("numini").value;
    const numerend= document.getElementById("numend").value;
    const tarcar= document.getElementById("tarica").value;
    const tarmot = document.getElementById("tarimo").value;
    const fracarr = document.getElementById("fraca").value;
    const framoto= document.getElementById("framo").value;
    const vrdiaca= document.getElementById("vrdica").value;
    const vrdiamo= document.getElementById("vrdimo").value;
    const vesemca= document.getElementById("vrseca").value;
    const vrsemmo= document.getElementById("vrsemo").value;
    const vrmesca= document.getElementById("vrmeca").value;
    const vrmesmo= document.getElementById("vrmemo").value;

 
    if(codigo2!==""){
        let objetoEnviar = {
            id_Configuracion:codigo2,
            nombreParqueadero:nombreparc,
            plazasCarro:plazcarr,
            plazasMoto:plazmot,
            prefijoFac:prefactu,     
            numeracionInic:numerini,
            numeracionEnd:numerend,
            tarifaCarro:tarcar,
            tarifaMoto:tarmot,
            fraccionCarro:fracarr,
            fraccionMoto:framoto,
            valorDiaCarro:vrdiaca,
            valorDiaMoto:vrdiamo,
            valorSemanaCarro:vesemca,
            valorSemanaMoto:vrsemmo,
            valorMesCarro:vrmesca,
            valorMesMoto:vrmesmo,
        }
        const apiCrear11 = "http://localhost:8080/setting/";

        fetch(apiCrear11,{
            method:"PUT",
            body:JSON.stringify(objetoEnviar),
            headers:{"Content-type":"application/json;charset=UTF-8"}
        })
        .then((respuesta)=>respuesta.json())
        .then((datos)=>{
            if (datos.hasOwnProperty("id_Configuracion")) {
                document.getElementById("confiMsgOk1").style.display="";
                document.getElementById("confiMsgError1").style.display="none";
                
            } else {
                document.getElementById("confiMsgOk1").style.display="none";
                document.getElementById("confiMsgError1").style.display="";
                
            }
        })
        .catch(
            (miError)=>{console.log(miError);}
        );

        //document.getElementById("formaCateCrear").reset();
        //document.getElementById("formaCateCrear").classList.remove("was-validated");
    }

}
function obtenerConfiActualizar(codigop) {
    const apiListaConfi = "http://localhost:8080/setting/list/"+codigop;
    const miPrimeraPromesa = fetch(apiListaConfi)
    .then((resultado) => resultado.json()).then((dato)=>{
        if(dato.hasOwnProperty("id_Configuracion"))
        {
            console.log(dato);
            document.getElementById("codi").value=dato.id_Configuracion;
            document.getElementById("nomparck").value=dato.nombreParqueadero;
            document.getElementById("plazca").value=dato.plazasCarro;
            document.getElementById("plazmo").value=dato.plazasMoto;
            document.getElementById("prefac").value=dato.prefijoFac;
            document.getElementById("numini").value=dato.numeracionInic;
            document.getElementById("numend").value=dato.numeracionEnd;
            document.getElementById("tarica").value=dato.tarifaCarro;
            document.getElementById("tarimo").value=dato.tarifaMoto;
            document.getElementById("fraca").value=dato.fraccionCarro;
            document.getElementById("framo").value=dato.fraccionMoto;
            document.getElementById("vrdica").value=dato.valorDiaCarro;
            document.getElementById("vrdimo").value=dato.valorDiaMoto;
            document.getElementById("vrseca").value=dato.valorSemanaCarro;
            document.getElementById("vrsemo").value=dato.valorSemanaMoto;
            document.getElementById("vrmeca").value=dato.valorMesCarro;
            document.getElementById("vrmemo").value=dato.valorMesMoto;

        }else{

        }

    }).catch((mierror)=> console.log(mierror) );

}
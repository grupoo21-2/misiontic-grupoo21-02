package com.api.project.EasyParking.model;

import com.sun.istack.NotNull;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "facturas")
public class Facturas implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfacturas")
    private Integer id_Facturas;

    @Column(name = "codigofactura")
    private String codigoFactura;

    @Column(name = "tipovehiculo")
    private String tipoVehiculo;

    @Column(name = "placa")
    private String placa;

    @Column(name = "tipocontrato")
    private String tipoContrato;

    @Column(name = "fechaentrada")
    private Date fechaEntrada;

    @Column(name = "fechasalida")
    private Date fechaSallida;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "fechafactura")
    private Date fechafactura;


    //@NotNull
    @ManyToOne
    @JoinColumn(name = "id_Personas")
    private Personas id_Persona;

    //@NotNull
    @ManyToOne
    @JoinColumn(name = "id_Plazas")
    private Plazas id_Plazas;

    public Facturas(){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Facturas facturas)) return false;
        return getTipoVehiculo().equals(facturas.getTipoVehiculo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTipoVehiculo());
    }
}

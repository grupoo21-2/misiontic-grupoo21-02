package com.api.project.EasyParking.model;

import lombok.Data;
import java.util.Date;
@Data
public class Register {
    private String placa;
    private String tipoVehiculo;
    private String tipoContrato;
    private Date fechaEntrada;
    private Double valor;
    private Personas id_Persona;
}

package com.api.project.EasyParking.DAO;

import com.api.project.EasyParking.model.Personas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonasDao extends CrudRepository<Personas, Integer> {

}

package com.api.project.EasyParking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table (name = "configuracion")
public class Setting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Configuracion")
    private Integer id_Configuracion;

    @Column(name = "nombreparqueadero")
    private String nombreParqueadero;

    @Column(name = "plazascarro")
    private Integer plazasCarro;

    @Column(name = "plazasmoto")
    private Integer plazasMoto;

    @Column(name = "prefijofac")
    private String prefijoFac;

    @Column(name = "numeracioninic")
    private Integer numeracionInic;

    @Column(name = "numeracionend")
    private Integer numeracionEnd;

    @Column(name = "valorhoracarro")
    private Double tarifaCarro;

    @Column(name = "valorhoramoto")
    private Double tarifaMoto;

    @Column(name = "valorfraccarro")
    private Double fraccionCarro;

    @Column(name = "valorfracmoto")
    private Double fraccionMoto;

    @Column(name = "valordiacarro")
    private Double valorDiaCarro;

    @Column(name = "valordiamoto")
    private Double valorDiaMoto;

    @Column(name = "valorsemanacarro")
    private Double valorSemanaCarro;

    @Column(name = "valorsemanamoto")
    private Double valorSemanaMoto;

    @Column(name = "valormescarro")
    private Double valorMesCarro;

    @Column(name = "valormesmoto")
    private Double valorMesMoto;

    public Integer getId_Configuracion() {
        return id_Configuracion;
    }




    public void setId_Configuracion(Integer idConfiguracion) {
        this.id_Configuracion = idConfiguracion;
    }


}

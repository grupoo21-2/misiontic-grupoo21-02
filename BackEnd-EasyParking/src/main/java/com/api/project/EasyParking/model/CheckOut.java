package com.api.project.EasyParking.model;

import lombok.Data;
import java.util.Date;

@Data
public class CheckOut {
    private String placa;
    private Date fechaSalida;
}

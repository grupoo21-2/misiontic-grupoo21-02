package com.api.project.EasyParking.model;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Data
@Entity
@Table(name = "personas")
public class Personas implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Personas")
    private Integer id_Persona;

    @Column(name = "nombres")
    private String nombre;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "tipo_Doc")
    private String tipoDoc;

    @Column(name = "num_Doc")
    private String numDoc;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "email")
    private String email;


}
